
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coba extends CI_Controller {

    public function index()
    {
      if($this->input->post('uploadFile')){
            $this->form_validation->set_rules('file', '', 'callback_file_check');

            if($this->form_validation->run() == true){
                //upload configuration
                $config['upload_path']   = 'uploads/files/';
                $config['allowed_types'] = 'gif|jpg|png|pdf';
                $config['max_size']      = 1024;
                $this->load->library('upload', $config);
                //upload file to directory
                if($this->upload->do_upload('file')){
                    $uploadData = $this->upload->data();
                    $uploadedFile = $uploadData['file_name'];
                    
                    /*
                     *insert file information into the database
                     *.......
                     */
                    
                    $data['success_msg'] = 'File has been uploaded successfully.';
                }else{
                    $data['error_msg'] = $this->upload->display_errors();
                }
            }
        }  
    }

}

/* End of file coba.php */
/* Location: ./application/controllers/coba.php */

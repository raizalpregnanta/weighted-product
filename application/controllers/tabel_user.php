<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_user
 extends CI_Controller {

function __construct(){

 		$this->db->load->model('mtabel_user');
 	}

	public function index()
	{
		$data['user']=$this->mtabel_user->GetUser();
		$this->template->load('halaman_admin/static_admin','halaman_admin/tabel_user',$data);
	}
}

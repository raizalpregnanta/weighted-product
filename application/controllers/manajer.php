<?php
session_start();
class Manajer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
		$this->load->model('mtabel_pegawai');

		if ($this->session->userdata('username')=="") {
			redirect('/login');
		}elseif($this->session->userdata('level') == 'admin'){
			redirect('/admin/index');
		}
	}

	public function index()
	{
		 $data = array(
					'error' => '',
					'username' => $this->session->userdata('username')
				);
		$this->template->load('manajer/static_hrd','manajer/dashbord_user');
	}

public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('/login');
	}
	
	public function tampil_pegawai(){
		 $data['pegawai'] = $this->mtabel_pegawai->tampil()->result();
        
        $this->template->load('manajer/static_hrd','manajer/tampil_pegawai',$data);
	}
}

		

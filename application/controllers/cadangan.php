<?php

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('mtabel_user');
    $this->load->model('mtabel_pegawai');
        //$this->load->library('upload');
		
	}

	public function index()
	{

		$this->template->load('admin/static_admin','admin/dashbord_admin');
	}
	public function tampil_user(){

		$data['user'] = $this->mtabel_user->tampil()->result();
		
		$this->template->load('admin/static_admin','admin/tabel_user',$data);
	}
    public function tampil_pegawai(){

        $data['pegawai'] = $this->mtabel_pegawai->tampil()->result();
        
        $this->template->load('admin/static_admin','admin/tampil_pegawai',$data);
    }

	

public function tambah_user(){

     $this->template->load('admin/static_admin','admin/tambah_user'); 

}
 public function tambah_userdb(){

            $this->form_validation->set_rules('user', 'username', 'trim|required|min_length[5]|max_length[12]');
            $this->form_validation->set_rules('pass', 'password', 'trim|required|min_length[5]|max_length[12]');
            $this->form_validation->set_rules('lev', 'level', 'trim|required|');    
      
        if ($this->form_validation->run() == FALSE)
        {
        
        $this->template->load('admin/static_admin','admin/tambah_user');
    
             }else{
         $this->load->library('upload');
        $nmfile = "file_".time(); //nama file + fungsi time
        $config['upload_path'] = './uploads/'; //Folder untuk menyimpan hasil upload
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '3072'; //maksimum besar file 3M
        $config['max_width']  = '5000'; //lebar maksimum 5000 px
        $config['max_height']  = '5000'; //tinggi maksimu 5000 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya

        $this->upload->initialize($config);
        
            if ($this->upload->do_upload('images'))
            {
                 $this->mtabel_user->tambah(); //akses model untuk menyimpan ke database
                //dibawah ini merupakan code untuk resize
                $config2['image_library'] = 'gd2'; 
                $config2['source_image'] = $this->upload->upload_path.$this->upload->file_name;
                $config2['new_image'] = './resize/'; // folder tempat menyimpan hasil resize
                $config2['maintain_ratio'] = TRUE;
                $config2['width'] = 100; //lebar setelah resize menjadi 100 px
                $config2['height'] = 100; //lebar setelah resize menjadi 100 px
                $this->load->library('image_lib',$config2); 

                //pesan yang muncul jika resize error dimasukkan pada session flashdata
                if ( !$this->image_lib->resize()){
                $this->session->set_flashdata('errors', $this->image_lib->display_errors('', ''));   
                    }
                //pesan yang muncul jika berhasil diupload pada session flashdata
                 $this->session->set_flashdata('info', 'Data Berhasil dimasukan');
                redirect('/admin/tampil_user'); //jika berhasil maka akan ditampilkan view upload
                  }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                 $this->session->set_flashdata('info', 'Gagal dimasukan');
                redirect('/admin/tambah_user');  //jika gagal maka akan ditampilkan form upload
            }

        }
     }

     public function tambah_pegawai(){
      $this->template->load('admin/static_admin','admin/tambah_pegawai');
     }

     public function tambah_pegawaidb(){

            $this->form_validation->set_rules('nama', 'nama', 'trim|required');
            $this->form_validation->set_rules('jk', 'jenis kelamin', 'trim|required');
            $this->form_validation->set_rules('tanggal', 'tgl', 'trim|required');  
            $this->form_validation->set_rules('alamat', 'alamat', 'trim|required'); 
             $this->form_validation->set_rules('email', 'email', 'valid_email|required|'); 
            $this->form_validation->set_rules('hp', 'no handpone', 'numeric|required|');  
            $this->form_validation->set_rules('pendidikan', 'pendidikan terakhir', 'trim|required|'); 
            $this->form_validation->set_rules('pengalaman', 'pengalaman kerja', 'trim|required|');
            $this->form_validation->set_rules('divisi', 'divisi', 'trim|required|');
          
        if ($this->form_validation->run() == FALSE)
        {
        
        $this->template->load('admin/static_admin','admin/tambah_pegawai');
    
             }else{
       
                 $this->mtabel_pegawai->tambah(); //akses model untuk menyimpan ke database
                  $this->session->set_flashdata('info', 'Data Berhasil dimasukan');
                 redirect('/admin/tampil_pegawai');
            }
	}
  
}
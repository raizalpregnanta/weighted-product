<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtabel_user extends CI_Model {

		
 	public function tambah(){

 		$a=$this->input->post('user');
 		$b=$this->input->post('pass');
 		$c=$this->upload->data('images');
 		$d=$this->input->post('lev');

                $data = array(
                  'username' =>$a,
                  'password' =>$b,
                  'images' =>$c['file_name'],
                  'level'=>$d
                );
		$query=$this->db->insert('user', $data);
		return $query;
		
		}

	public function tampil(){

		$query=$this->db->get('user');
		return $query;
	}

	public function ceklogin($user,$pass){
		$this->db->where('username',$user);
		$this->db->where('password',$pass);

		return $this->db->get('user')->row();

	}

}
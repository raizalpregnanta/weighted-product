  <div class="x_panel">
                  <div class="x_title">
                    <h2>Tabel Pegawai</small></h2>
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
            
                    <?php if($this->session->flashdata('info')): ?>

                   <div class="alert alert-success alert-dismissible fade in">
                   <?php echo $this->session->flashdata('info'); ?> </div> 

                 <?php endif; ?>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama</th>
                          <th>Jenis Kelamin</th>
                           <th>Tanggal Lahir</th>
                          <th>Alamat</th>
                          <th>Email</th>
                          <th>No Handpone</th>
                          <th>Pendidikan Terakhir</th>
                          <th>Pengalaman Kerja</th>
                          <th>Divisi</th>
                          <th>Aksi</th>
                        
                        </tr>

                       
                        <?php 
                        $no=1;
                        foreach ($pegawai as $peg) {
                        ?> 
                        <tr>
                          <td><?php echo $no++ ?></td>
                           <td><?php echo $peg->nama_pelamar ?></td> 
                            <td><?php echo $peg->jk ?></td>
                            <td><?php echo $peg->tanggal_lahir ?></td>
                            <td><?php echo $peg->alamat ?></td>
                            <td><?php echo $peg->email ?></td>
                            <td><?php echo $peg->no_hp ?></td>
                            <td><?php echo $peg->pendidikan ?></td>
                            <td><?php echo $peg->pengalaman_kerja ?></td>
                            <td><?php echo $peg->divisi ?></td>
                             <td>
                            <div>
                              
                            <a class="fa fa-edit"><?php echo anchor('admin/edit'.$peg->id_pelamar,'  edit') ?></a> |
                              <a class="fa fa-trash"> <?php echo anchor('admin/hapus'.$peg->id_pelamar,'  delete') ?></a> </div>   
                              </td>    

                        </tr>
                        
                        <?php 
                        }

                         ?>
                      </thead>
                      <tbody>
                        </tr>
                      </tbody>
                    </table>
          
          
                  </div>
                
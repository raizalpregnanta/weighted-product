 <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

                <p align="justify">Rumah Web Indonesia memiliki beberapa paket web hosting yang dapat anda pilih. Layanan web hosting dengan teknologi cloud computing yang lebih handal. Tersedia pilihan lokasi server di Indonesia, Singapore, dan US. Selain sebagai tempat penyimpanan data website, account hosting Anda juga dilengkapi dengan fasilitas email dengan nama domain sendiri (nama@domainsaya.com). 
                Berikut adalah detail fitur email dari layanan hosting Rumahweb:
                </p>
                <p>
                    <ul>
                        <li>Akses POP3, IMAP, & SMTP</li>
                        <li>Webmail: Squirrelmail, Horde, & Roundcube</li>
                        <li>Kompatibel dg iPhone, Android, BB, dan platform lain</li>
                        <li>Email forwarding</li>
                        <li>Anti Spam dengan SpamAssasin</li>
                        <li>Modifikasi MX</li>
                    </ul>
                </p>

                <p align="justify">Dengan berbagai fasilitas yang rumahweb sediakan, maka akan membuat anda untuk dapat selektif dalam memilih. Maka dari itu, Web Hosting Selected hadir untuk membantu anda dalam memberikan pilihan terbaik dari berbagai paket web hosting yang disediakan rumahweb. Web Hosting Selected adalah website yang  membantu anda untuk memilih paket hosting sesuai dengan kriteria atau fasilitas paket web hosting yang ada. Sehingga anda, tidak salah memilih untuk menentukan pilihan. Kami, dapat memberikan alternatif paket hosting dan memilih paket hosting yang terbaik untuk kebutuhan web hosting anda. </p>
            </div>
        </div>
    </div>
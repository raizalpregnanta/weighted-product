/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rumahweb

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-07-20 01:38:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `level` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'admin', 'admin');
INSERT INTO `user` VALUES ('2', 'faizal', 'donio', 'admin');
INSERT INTO `user` VALUES ('3', 'sinta', 'aku', 'admin');
INSERT INTO `user` VALUES ('6', 'anita', 'anita', 'manajer');

-- ----------------------------
-- Table structure for wp_alternatif
-- ----------------------------
DROP TABLE IF EXISTS `wp_alternatif`;
CREATE TABLE `wp_alternatif` (
  `id_alternatif` int(11) NOT NULL AUTO_INCREMENT,
  `nama_alternatif` varchar(50) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_hp` int(11) NOT NULL,
  `pendidikan` varchar(20) NOT NULL,
  `pengalaman_kerja` varchar(500) NOT NULL,
  `divisi` varchar(10) NOT NULL,
  `file` varchar(50) NOT NULL,
  `vektor_s` double DEFAULT NULL,
  `vektor_v` double DEFAULT NULL,
  PRIMARY KEY (`id_alternatif`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wp_alternatif
-- ----------------------------
INSERT INTO `wp_alternatif` VALUES ('1', 'rina', 'pria', '2017-06-16', 'jalan mangga no 23 malang', 'ajhsa@ajsa.com', '2147483647', 'S2', 'ajksjsahaas', 'billing', 'file_1497534383.rar', '3004.8057646267', '0.8327400954878474');
INSERT INTO `wp_alternatif` VALUES ('2', 'doni', 'wanita', '2017-07-20', 'jalan pegangsaan timur', 'ajsh@ajaks.com', '2147483647', 'S2', 'skjksd \r\nsdkjds\r\najksak', 'billing', 'file_1499671677.rar', '292.00684370257', '0.08092563245537858');
INSERT INTO `wp_alternatif` VALUES ('3', 'Raizal', 'wanita', '1992-09-19', 'kepo', 'raizal@mail.com', '2147483647', 'D3', 'BANYAK BOS', 'developer', 'file_1500488059.zip', '311.52303073514', '0.08633427205677394');
INSERT INTO `wp_alternatif` VALUES ('4', 'Testing bos', 'wanita', '1992-09-19', 'kepo', 'kepo@mail.com', '81203812', 'S2', 'aksmdalsmdlka', 'teknisi', 'file_1500489230.zip', '0', '0');

-- ----------------------------
-- Table structure for wp_kriteria
-- ----------------------------
DROP TABLE IF EXISTS `wp_kriteria`;
CREATE TABLE `wp_kriteria` (
  `id_kriteria` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(50) NOT NULL,
  `tipe_kriteria` enum('Cost','Benefit') NOT NULL DEFAULT 'Benefit',
  `bobot` double DEFAULT '0',
  PRIMARY KEY (`id_kriteria`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wp_kriteria
-- ----------------------------
INSERT INTO `wp_kriteria` VALUES ('2', 'Fitur', 'Benefit', '20');
INSERT INTO `wp_kriteria` VALUES ('5', 'Populer', 'Benefit', '12.5');
INSERT INTO `wp_kriteria` VALUES ('6', 'Purna Jual', 'Benefit', '12.5');
INSERT INTO `wp_kriteria` VALUES ('7', 'Kualitas', 'Benefit', '25');
INSERT INTO `wp_kriteria` VALUES ('8', 'Harga', 'Cost', '20');
INSERT INTO `wp_kriteria` VALUES ('9', 'Keawetan', 'Benefit', '10');

-- ----------------------------
-- Table structure for wp_nilai
-- ----------------------------
DROP TABLE IF EXISTS `wp_nilai`;
CREATE TABLE `wp_nilai` (
  `id_nilai` int(50) NOT NULL AUTO_INCREMENT,
  `id_alternatif` int(255) DEFAULT NULL,
  `id_kriteria` int(100) NOT NULL,
  `nilai` double NOT NULL,
  PRIMARY KEY (`id_nilai`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of wp_nilai
-- ----------------------------
INSERT INTO `wp_nilai` VALUES ('16', '1', '2', '2400');
INSERT INTO `wp_nilai` VALUES ('17', '1', '5', '2700');
INSERT INTO `wp_nilai` VALUES ('18', '1', '6', '3000');
INSERT INTO `wp_nilai` VALUES ('19', '1', '7', '3200');
INSERT INTO `wp_nilai` VALUES ('20', '1', '8', '3400');
INSERT INTO `wp_nilai` VALUES ('21', '1', '9', '3600');
INSERT INTO `wp_nilai` VALUES ('22', '2', '2', '100');
INSERT INTO `wp_nilai` VALUES ('23', '2', '5', '200');
INSERT INTO `wp_nilai` VALUES ('24', '2', '6', '300');
INSERT INTO `wp_nilai` VALUES ('25', '2', '7', '400');
INSERT INTO `wp_nilai` VALUES ('26', '2', '8', '500');
INSERT INTO `wp_nilai` VALUES ('27', '2', '9', '600');
INSERT INTO `wp_nilai` VALUES ('28', '3', '2', '200');
INSERT INTO `wp_nilai` VALUES ('29', '3', '5', '300');
INSERT INTO `wp_nilai` VALUES ('30', '3', '6', '500');
INSERT INTO `wp_nilai` VALUES ('31', '3', '7', '900');
INSERT INTO `wp_nilai` VALUES ('32', '3', '8', '100');
INSERT INTO `wp_nilai` VALUES ('33', '3', '9', '300');
